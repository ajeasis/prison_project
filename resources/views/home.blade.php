@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-body">
                <center><table width="500" border="0" cellpadding="5">
                <tr>
                    <td align="center" valign="center">
                        <a href="{{ route('prisoners.index') }}"><img src="https://i.imgur.com/pDZqw30.png"><br></a>&emsp;&emsp;
                        <br>
                        Prisoner List
                    </td>

                    <td align="center" valign="center">
                        @can('Add User')
                        <a href="{{ route('users.index') }}"><img src="https://i.imgur.com/hF9S6Pn.png"><br></a>&ensp;
                        <br>
                        User Administration
                        @endcan</div>
                    </td>
                </tr>
                </table></center>
            </div>
        </div>
        <center><img src="https://media.giphy.com/media/DS89v1NqpzCqA/giphy.gif" alt="home logo"></center>
    </div>
</div>
@endsection