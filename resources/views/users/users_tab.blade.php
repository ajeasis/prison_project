<div class='container'>
  @can('Add User')
    <div class="row pull-left">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> User
        </button>
    </div>
  @endcan
    <table class="row table table-hover">
        <thead>
            <tr>
              <th>Name</th>
              <th>E-mail</th>
              <th>Role</th>
              <th>Status</th>
              <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
              <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->roles()->pluck('name')->implode(', ') }}</td>
                @if ($user->status == 0)
                  <td class="text-success"><b>Active</b></td>
                @else
                  <td class="text-danger"><b>Inactive</b></td>
                @endif
                <td>
                  <div class="btn-group pull-right" role="group">
                      @can('Edit User')
                          <button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editUser" data-id="{{$user->id}}" data-name="{{$user->name}}" data-email="{{$user->email}}" data-role="{{$user->roles()->pluck('role_id')->implode('')}}" data-status="{{$user->status}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                          </button>
                      @endcan 
                  </div>
                </td>
              </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('modals')
@parent
    <!-- Add User Modal -->
    <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Add User</b></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    {{-- Edit User Modal --}}
    <div id="editUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-primary">
              <div class="panel-heading">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="panel-title" id="myModalLabel"><b>Edit User</b></h4>
              </div>
            <div class="modal-body">
              @if(isset($user))
                {!! Form::model($users, ['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}
                    {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', null, ['id' => 'un', 'class' => 'form-control', 'required' => '', 'maxlength' => '15']) !!}

                    {!! Form::label('email', 'E-Mail:') !!}
                    {!! Form::text('email', null, ['id' => 'ue', 'class' => 'form-control', 'required' => '', 'maxlength' => '30']) !!}

                    {!! Form::label('role_id', 'Role:') !!}
                    {!! Form::select('role_id', $roles->pluck('name'), null, ['id' => 'ur', 'class' => 'form-control', 'required' => '', 'placeholder' => 'Select one']) !!}

                    {!! Form::label('status', 'Status:') !!}
                    {!! Form::select('status', ['Active', 'Inactive'], $user->status, ['id' => 'us', 'class' => 'form-control', 'required' => '']) !!}

                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                      {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                    </div>
                {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            var $user_id = $(this).data('id');
            console.log($user_id)
            $('.form-horizontal').show();
            if ($user_id == 1) {
                $('#un').prop("disabled", true);
                $('#ue').prop("disabled", true);
                $('#ur').prop("disabled", true);
                $('#us').prop("disabled", true);
            } else {
                $('#un').prop("disabled", false);
                $('#ue').prop("disabled", false);
                $('#ur').prop("disabled", false);
                $('#us').prop("disabled", false);
            }
            $('#uid').val($(this).data('id'));
            $('#un').val($(this).data('name'));
            $('#ue').val($(this).data('email'));
            $('#ur').val($(this).data('role')-1);
            $('#us').val($(this).data('status'));
            $('#myModal').modal('show');
        });
    </script>
@endsection