@extends('layouts.layout')

@section('content')

	<div class="container">
		<h3>List of prisoner</h3>
		<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Add prisoner
        </button>

		<table class="table table-hover" id="myTable">
			<thead>
				<tr>
					<td>Lastname</td>
					<td>Firstname</td>
					<td>Address</td>
					<td>Birthdate</td>
					<td>Sentence</td>
					<td>Offense</td>
					<td>Date Incarcerated</td>
					<td>Cell Block</td>
					<td>Cell Room</td>
				</tr>
			</thead>
			<tbody>
				@foreach($prisoners as $prisoner)
					<tr>
						<td>{{ $prisoner->lastname }}</td>
						<td>{{ $prisoner->firstname }}</td>
						<td>{{ $prisoner->address }}</td>
						<td>{{ $prisoner->birthdate }}</td>
						<td>{{ $prisoner->sentence }}</td>
						<td>{{ $prisoner->offense }}</td>
						<td>{{ $prisoner->dateinc }}</td>
						<td>{{ $prisoner->cellblock }}</td>
						<td>{{ $prisoner->cellroom }}</td>
						<td>
							<div class="btn-group pull-right">
                          		<button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editprisoner" data-id="{{$prisoner->id}}" data-lastname="{{$prisoner->lastname}}" data-firstname="{{$prisoner->firstname}}" data-address="{{$prisoner->address}}" data-birthdate="{{$prisoner->birthdate}}" data-sentence="{{$prisoner->sentence}}" data-offense="{{$prisoner->offense}}" data-dateinc="{{$prisoner->dateinc}}" data-cellblock="{{$prisoner->cellblock}}" data-cellroom="{{$prisoner->cellroom}}" >
                            	<span class="glyphicon glyphicon-pencil"></span>
                          		</button>
                  			</div>
					</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection


@section('modals')
@parent
<div id="editprisoner" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Edit Prisoner</b></h4>
          </div>
        <div class="modal-body">
        
        	 @if(isset($prisoner->id))
            {!! Form::model($prisoner, ['route' => ['prisoners.update', $prisoner->id], 'method' => 'PUT']) !!}
            {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

            {!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['id' => 'lname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['id' => 'fname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['id' => 'uaddress', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['id' => 'ubirthdate', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Sentence') !!}
			{!! Form::text('sentence', null, ['id' => 'usentence', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Offense') !!}
			{!! Form::text('offense', null, ['id' => 'uoffense', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Date Incarcerated') !!}
			{!! Form::date('dateinc', null, ['id' => 'udateinc', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Cell Block') !!}
			{!! Form::text('cellblock', null, ['id' => 'ucellblock', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Cell Room') !!}
			{!! Form::text('cellroom', null, ['id' => 'ucellroom', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                  {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                </div>
            	{!! Form::close() !!}
            	@endif
        </div>
      </div>
    </div>
</div>    

{{-- Add User Modal --}}
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Add Prisoner</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route' => 'prisoners.store', 'Method' => 'POST']) !!}

			{!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Sentence') !!}
			{!! Form::text('sentence', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Offense') !!}
			{!! Form::text('offense', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Date Incarcerated') !!}
			{!! Form::date('dateinc', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Cell Block') !!}
			{!! Form::text('cellblock', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Cell Room') !!}
			{!! Form::text('cellroom', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	        {!! form::submit('Save', ['class' => 'btn btn-success'])!!}

	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#lname').val($(this).data('lastname'));
            $('#fname').val($(this).data('firstname'));
            $('#uaddress').val($(this).data('address'));
            $('#ubirthdate').val($(this).data('birthdate'));
            $('#usentence').val($(this).data('sentence'));
            $('#uoffense').val($(this).data('offense'));
            $('#udateinc').val($(this).data('dateinc'));
            $('#ucellblock').val($(this).data('cellblock'));
            $('#ucellroom').val($(this).data('cellroom'));


            $('#editprisoner').modal('show');
        });
    </script>
@endsection

