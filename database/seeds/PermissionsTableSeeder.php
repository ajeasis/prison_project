<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Insert into Users Table
		DB::table('users')->insert([
            'name' => 'Super Administrator',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123456.'),
            'status' => 0,
        ]);


		//Insert into Roles Table
        DB::table('roles')->insert([
            'name' => 'Administrator',
            'description' => 'All Rights',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);


        //Insert into Permissions Table, all permissions are added here
        DB::table('permissions')->insert([
            'name' 			=> 'Add Site',
            'description' 	=> 'Can create a new site/project',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'name' 			=> 'Edit Site',
            'description' 	=> 'Can edit a site/project',
            'created_at' 	=> \Carbon\Carbon::now(),
            'updated_at' 	=> \Carbon\Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'name'          => 'Add User',
            'description'   => 'Add User',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'name'          => 'Add Role',
            'description'   => 'Can create a user role',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);

        DB::table('permissions')->insert([
            'name'          => 'Edit Role',
            'description'   => 'Can edit a role permissions',
            'created_at'    => \Carbon\Carbon::now(),
            'updated_at'    => \Carbon\Carbon::now()
        ]);

        //Assign user Super Administrator the role Administrator
        $user = User::find(1);
        $user->roles()->attach(1);

        //Assign role Administrator all the permissions.
        $role = Role::find(1);
        $role->permissions()->attach([1,2,3,4,5]);
    }
}
