<div class='container'>
  @can('Add Role')
    <div class="row pull-left">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addRole">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Role
        </button>
    </div>
  @endcan
    <table class="row table table-hover">
        <thead>
            <tr>
              <th><class="form-control">Name</th>
              <th><class="form-control">Description</th>
              <th><class="form-control">Permissions</th>
              <th><class="form-control">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($roles as $role)
              <tr>
                <td>{{ $role->name }}</td>
                <td>{{ $role->description }}</td>
                <td>{{ $role->permissions()->pluck('name')->implode(', ') }}</td>
                <td>
                  <div class="btn-group pull-left" role="group">
                    @can('Edit Role')
                      <button class="edit-role btn btn-success" data-toggle="modal" data-target="#editRole" data-id="{{ $role->id }}" data-name="{{ $role->name }}" data-url="{{ route('users.edit', $role->id) }}">
                        <span class="glyphicon glyphicon-pencil"></span>
                      </button>
                    @endcan
                  </div>
                </td>
              </tr>
            @endforeach
        </tbody>
    </table>
</div>

@section('modals')
@parent
  <!-- Add Role Modal -->
  <div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="panel panel-primary">
        <div class="panel-heading">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title" id="myModalLabel"><b>Add New Role</b></h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'data-parsely-validate' => '']) !!}
              {!! Form::label('name', 'Role Name:') !!}
              {!! Form::text('name', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '15']) !!}

              {!! Form::label('description', 'Role Description:') !!}
              {!! Form::text('description', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '25']) !!}

              {!! Form::label('assginPermission', 'Assgin Permission:') !!}            
              <div class='form-group' style="margin-left: 20px">
                @foreach ($userpermissions as $userpermission)
                    {{ Form::checkbox('userpermissions[]',  $userpermission->id) }}
                    {{ Form::label($userpermission->name, $userpermission->description) }}<br>
                @endforeach
              </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
              {!! Form::submit('Save Role', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>

  {{-- Edit Site Modal --}}
  <div id="editRole" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="panel panel-primary">
            <div class="panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="panel-title" id="myModalLabel"><b>Edit Role</b></h4>
            </div>
          <div class="modal-body">
            @if(isset($role))
              {!! Form::model($roles, ['route' => ['users.update_role'], 'method' => 'POST']) !!}
                  {!! Form::hidden('id', null, ['id' => 'rid', 'class' => 'form-control', 'required' => '']) !!}

                  {!! Form::label('name', 'Role:') !!}
                  {!! Form::text('name', null, ['id' => 'rn', 'class' => 'form-control', 'required' => '', 'maxlength' => '15']) !!}

                  {!! Form::label('description', 'Description:') !!}
                  {!! Form::text('description', null, ['id' => 'rd', 'class' => 'form-control', 'required' => '', 'maxlength' => '25']) !!}

                  {!! Form::label('assginPermission', 'Assgin Permission:') !!}

                  <div class='form-group' style="margin-left: 20px">
                    <table id="role-permissions-table" class="table table-hover hidden">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody id="role-permissions-body">
                      </tbody>
                    </table>
                  </div>

                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                    {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                  </div>
              {!! Form::close() !!}
            @endif
          </div>
        </div>
      </div>
  </div>

@endsection

@section('scripts')
@parent
  <script type="text/javascript">
    $(function() {
      var rolePermissions = $('#role-permissions-table');
      var rolePermissionsBody = $('#role-permissions-body');
      var rp = [];

      rolePermissionsBody.empty();

      $('.edit-role').click(function() {
        rolePermissionsBody.empty();
        rp.length = 0;
        $.ajax({
          type: 'GET',
          url: $(this).data('url'),
          success: function(data) {
            $('#rid').val(data.role.id),
            $('#rn').val(data.role.name),
            $('#rd').val(data.role.description)

            for (x in data.role.permissions) {
              rp.push(data.role.permissions[x].pivot.permission_id)
            }

            for (permissions in data.user_permissions) {
              var in_array = $.inArray(data.user_permissions[permissions].id, rp);
              if (in_array == -1) {
                var checked = '';
              } else {
                checked = 'checked';
              }
              var row = '<tr class="role-permissions-row">' +
                        '<td><input type="checkbox" name="userpermissions[]" value=' + data.user_permissions[permissions].id + ' ' + checked +'></td>' + 
                        '<td>' + data.user_permissions[permissions].description + '</td>' +
                        '</tr>';
              rolePermissionsBody.append(row);
            }
            rolePermissions.removeClass('hidden');
          }
        });
      });
    });
  </script>
@endsection