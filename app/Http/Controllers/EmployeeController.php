<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Session;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employee::paginate(5);

        return view('employee.index')->withEmployees($employees);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('employee.employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $employee = New Employee;

        $employee->barcode = self::generateBarcodeNumber();
        $employee->lastname = $request -> lastname;
        $employee->firstname = $request -> firstname;
        $employee->address = $request -> address;
        $employee->birthdate = $request -> birthdate;

        $employee->save();

        $lastEmp = 'User added successfully. Employee Code:' . " " . (string)$employee->barcode;

        return redirect()->route('employee.index')->with('message', $lastEmp);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$employee = Employee::find($id);

        return View::make('employee.index')
            ->with('employee', $employee);*/

        $role_permissions = Role::with('Permissions')->findOrFail($id);
        $user_permissions = Permission::all();

        return response()->json(['role' => $role_permissions, 'user_permissions' => $user_permissions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($request->input('id'));

        $employee->id           = $request->id;
        $employee->lastname     = $request->lastname;
        $employee->firstname    = $request->firstname;
        $employee->address      = $request->address;
        $employee->birthdate    = $request->birthdate;

        $employee->save();

        return redirect()
            ->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateBarcodeNumber() {
        $number = strval(mt_rand(100000, 999999)); // better than rand()

        // call the same function if the barcode exists already
        if (self::barcodeNumberExists($number)) {
            return self::generateBarcodeNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    public function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Employee::whereBarcode($number)->exists();
    }
}
