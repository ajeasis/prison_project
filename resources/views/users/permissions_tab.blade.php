<div class='container'>
    <table class="row table table-hover">
        <thead>
            <tr>
              <th><class="form-control">Name</th>
              <th><class="form-control">Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($userpermissions as $userpermission)
              <tr>
                <td>{{ $userpermission->name }}</td>
                <td>{{ $userpermission->description }}</td>
              </tr>
            @endforeach
        </tbody>
    </table>
</div>