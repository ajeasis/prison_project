<?php

Auth::routes();

Route::get('/', 'HomeController@index');
//Route::group(['middleware' => 'auth'], function() {
Route::group(['middleware' => 'auth'], function() {
	Route::resource('users', 'UserController', ['except' => ['create', 'show', 'destroy']]);
	Route::post('updateroles', ['uses' => 'UserController@update_role', 'as' => 'users.update_role']);
});

Route::resource('prisoners', 'PrisonerController');
//});
