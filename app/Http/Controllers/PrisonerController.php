<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prisoner;

class PrisonerController extends Controller
{
    public function index()
    {
        //
        $prisoners = Prisoner::all();

        return view('prisoners.index')->withPrisoners($prisoners);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $prisoner = New Prisoner;

        $prisoner->lastname = $request -> lastname;
        $prisoner->firstname = $request -> firstname;
        $prisoner->address = $request -> address;
        $prisoner->birthdate = $request -> birthdate;
        $prisoner->sentence = $request -> sentence;
        $prisoner->offense = $request -> offense;
        $prisoner->dateinc = $request -> dateinc;
        $prisoner->cellblock = $request -> cellblock;
        $prisoner->cellroom = $request -> cellroom;

        $prisoner->save();

        return redirect()->route('prisoners.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prisoner = Prisoner::find($id);

        $prisoner->id           = $request->id;
        $prisoner->lastname     = $request->lastname;
        $prisoner->firstname    = $request->firstname;
        $prisoner->address      = $request->address;
        $prisoner->birthdate    = $request->birthdate;
        $prisoner->sentence 	= $request->sentence;
        $prisoner->offense      = $request->offense;
        $prisoner->dateinc      = $request->dateinc;
        $prisoner->cellblock 	= $request->cellblock;
        $prisoner->cellroom 	= $request->cellroom;

        $prisoner->save();

        return redirect()
            ->route('prisoners.index');
    }

}
