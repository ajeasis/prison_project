<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Permissions\HasPermissionsTrait;

class Role extends Model
{

	use HasPermissionsTrait;

	protected $fillable = [
    	'name', 'description'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}
